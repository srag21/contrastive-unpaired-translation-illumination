python train.py \
    --name CUT_SIGF_IllRBFB_SegRAFB \
    --CUT_mode CUT \
    --direction AtoB \
    --dataroot /projects/sraghavan@xsede.org/GlaucomaIlluminationEnhancement/retina_datasets/new_datasets/SIGF-database-merged-resized_forCUT \
    --batch_size 32 \
    --verbose \
    --dataset_mode unaligned 
