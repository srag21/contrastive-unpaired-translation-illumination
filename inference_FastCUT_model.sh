python inference.py \
    --dataroot ./datasets/SIGF-database-merged-resized_forCUT  \
    --name FastCUT_SIGF_IllRBFB_SegRAFB  \
    --CUT_mode CUT \
    --phase train