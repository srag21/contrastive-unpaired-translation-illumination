python train.py \
    --name FastCUT_SIGF_IllRBFB_SegRAFB \
    --CUT_mode FastCUT \
    --direction AtoB \
    --dataroot ./datasets/SIGF-database-merged-resized_forCUT \
    --batch_size 32 \
    --verbose \
    --dataset_mode unaligned 
