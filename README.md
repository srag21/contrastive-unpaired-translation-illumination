
# Contrastive Unpaired Translation (CUT)

## Setup
```
git clone https://gitlab.com/srag21/contrastive-unpaired-translation-illumination.git
cd contrastive-unpaired-translation-illumination
conda env create -f CUT_Illumination_RetinaSeg.yml
conda activate CUT_Illumination_RetinaSeg
```
## Training
To train the FASTCut model with on the SIGF dataset, download the dataset from this [link](https://drive.google.com/file/d/1xSpGGkwcLlv1zoWKNvmaaKYTtlrB5bbE/view?usp=sharing). 

Then, run the training script by: 
```
bash train_FastCUT_model.sh
```

## Inference
To perform inference after training, run 
```
bash inference_FastCUT_model.sh
```
