python train.py \
    --name FastCUT_SIGF \
    --CUT_mode FastUT \
    --direction AtoB \
    --dataroot /projects/sraghavan@xsede.org/GlaucomaIlluminationEnhancement/retina_datasets/new_datasets/SIGF-database-merged-resized_forCUT \
    --batch_size 32 \
    --verbose \
    --dataset_mode unaligned \
    #--continue-train \