import os
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
from docopt import docopt
import pandas as pd
import fastai
from fastai.vision import *
import pretrainedmodels
from sklearn.metrics import *
from fastai.callbacks import *
import math
import time
import numpy as np

#fix
from .SegmentationUtils import SegmentationDataset
from .SegmentationUtils import SegmentationModel
from .SegmentationUtils import SoftDiceLoss
from .SegmentationUtils import CrossEntropy
from .unet_model import UNet

# from SegmentationUtils import SegmentationDataset
# from SegmentationUtils import SegmentationModel
# from SegmentationUtils import SoftDiceLoss
# from SegmentationUtils import CrossEntropy
# from unet_model import UNet


from torchvision.utils import save_image


import cv2
from scipy.ndimage import label, generate_binary_structure

import torch
import torch.nn as nn

# from fastai.data.external import *



tfms_test = get_transforms(do_flip = False,max_warp = None,max_rotate=0.0,max_zoom=1.0,max_lighting=0.0)
tfms = get_transforms(do_flip = False, 
max_rotate = 5.0, max_zoom = 1.2, max_lighting=0.5,max_warp = None)
num_workers = 8
bs = 64
size = 320
cont_thresh = 50
thresh = 1

def trainable(net, trainable):
    for param in net.parameters():
        param.requires_grad = trainable

def Retina_Seg_load_model(x): 


    output_df = pd.DataFrame()
    output_df['Files'] = os.listdir(x.img_dir)
    output_df['Dummy'] = np.random.rand(len(output_df['Files']))
    col = "Dummy"

    imgs = (ImageList.from_df(df=output_df, path=x.img_dir)
                              .split_none()
                              #.split_by_idx(valid_idx)
                              .label_from_df(cols=col)
                              .databunch(num_workers = num_workers,bs=bs))
    
    learners = []
    #num_models = int(arguments['<num_models>'])

    for i in range(x.num_models):
        model = UNet(3,1,bilinear=False)
        learn = Learner(imgs, model)
        #learn.model_dir = ""
        learn.load(x.model_dir  + "_" + str(i))
        
        learn.model.eval()
        learn.model.cuda()
        learners.append(learn.model)

    return learners

class SegmentationLoss(nn.Module):

  #def __init__(self, model_dir, img_dir, num_models=1, cleanISO=False):
  def __init__(self, num_models=1, cleanISO=False):
    super(SegmentationLoss, self).__init__()
    img_dir= "/projects/sraghavan@xsede.org/GlaucomaIlluminationEnhancement/source_code/Retina-Seg/test_images/"
    model_dir = "/projects/sraghavan@xsede.org/GlaucomaIlluminationEnhancement/source_code/Retina-Seg/model/UNet_Ensemble"
    self.num_models = num_models
    self.model_dir = model_dir
    self.img_dir = img_dir
    self.cleanISO = cleanISO


    self.model = Retina_Seg_load_model(self)
    for m in self.model:
        trainable(m, False)

    #self.loss = SoftDiceLoss().cuda()
    self.loss = CrossEntropy().cuda() 

    self.img_dim = (320, 320)
    # self.batch_size = batch_size
    #self.resize = transforms.Resize(320, ResizeMethod.Squish)
  #load model and return it

  


  def get_output(self, img):
    size = img.shape[-1]
    all_imgs = torch.zeros(self.num_models,3,size,size)
    for j in range(self.num_models):
        #curr = open_image(os.path.join(image_dir,output_df.iloc[i,0]))
        curr = img

        #Transform to correct size          
        # curr = curr.resize(size).px
        #curr = curr.px
        
        #Apply segmentation model j
        mask = self.model[j](curr.view(1,3,size,size).cuda())
        #mask = self.model[j](curr.cuda())
        
        mask = 1-torch.round(mask*thresh)

        mask = torch.mode(mask,dim=1)
        tmp = mask.values.cpu().detach().numpy()
        mask_for_dice = 1-tmp
        if(self.cleanISO):
            arr,nf = label(tmp)
            unique,counts = np.unique(arr.flatten(),return_counts=True)
            lbls_keep = unique[counts < cont_thresh]
            tmp[np.isin(arr,lbls_keep)] = 0
            mask = torch.Tensor(tmp)
        else:
            #mask = mask.values.cpu().detach()
            mask = mask.values

        mask = mask.repeat(1,3,1,1)
        
        
        mask = 1 - mask
        #Store each image
        all_imgs[j,:,:,:] = mask.clone().detach().cpu()
        mask = mask*curr.view(1,3,size,size)
                      
    #Majority vote for pixel output
    final_mask = torch.mean(all_imgs,0) #dim should be 1
    return final_mask.view(3,size,size)#.clone().detach().cpu()
    

  def get_output_batched(self, img):
    size = img.shape[-1]
    # print(f"shape of img = {img.shape}")
    all_imgs = torch.zeros(self.num_models, img.shape[0], img.shape[2], img.shape[3])
    for j in range(self.num_models):
        curr = img

        mask = self.model[j](curr.cuda())
        # print(f"mask shape  = {mask.size()}")
        
        mask = 1-torch.round(mask*thresh)
        # print(f"mask shape  after round = {mask.size()}")

        mask = torch.mode(mask,dim=1)
        mask = mask.values

        # print(f"mask values size  = {mask.size()}")


        #mask = mask.repeat(1,3,1,1)
        #print(f"mask shape after repeat  = {mask.size()}")
        
        
        mask = 1 - mask
        # print(f"mask shape after repeat  = {mask.size()}")
        # print(f"all_images shape   = {all_imgs[0, :, :, :].size()}")
        
        #Store each image
        # all_imgs[j,:,:,:] = mask.clone().detach().cpu()
        #all_imgs[j,:,:,:] = mask.clone()
        #mask_copy = mask.clone().detach().resize_(self.batch_size, 3, size, size)
        # print(f"mask shape after resize  = {mask.size()}")
        all_imgs[j, :, :, :] = mask#mask.clone().detach().resize_(self.batch_size, 3, size, size)
        
        #mask = mask*curr.view(1,3,size,size)
                      
    #Majority vote for pixel output
    final_mask = torch.mean(all_imgs,0)
    #return final_mask.view(3,size,size)#.clone().detach().cpu()
    # print(f"final mask size = {final_mask.size()}")
    return final_mask

  def __call__(self, real_B, fake_B):
    # print(type(real_B))
    #  print(type(np.array(real_B)))
     
    #  real_B = cv2.resize(np.array(real_B).astype(np.uint8), (320, 320), interpolation = cv2.INTER_AREA)
    #  fake_B = cv2.resize(np.array(fake_B).astype(np.uint8), (320, 320), interpolation = cv2.INTER_AREA)
     
    #  real_B = Image(pil2tensor(real_B, dtype=np.float32).div_(255))
    #  fake_B = Image(pil2tensor(fake_B, dtype=np.float32).div_(255))
    #  real_B.resize(torch.Size([real_B.shape[0], 320, 320]), resize_method=ResizeMethod.SQUISH).refresh()
    #  fake_B.resize(torch.Size([fake_B.shape[0],320, 320]), resize_method=ResizeMethod.SQUISH).refresh()
    #  real_B = self.resize(real_B)
    #  fake_B = self.resize(fake_B)
    #  real_B = Image.reshape(real_B, 320, 320, resample=0)
    #  fake_B = Image.reshape(fake_B, 320, 320, resample=0)

     #print(f"type of i1 = {type(real_B)}")
     #print(f"size of image = {real_B.shape}")

     gt = self.get_output_batched(real_B)
     pred = self.get_output_batched(fake_B)
     #loss_Seg = self.loss(pred.unsqueeze(0), gt.unsqueeze(0))
     loss_Seg = self.loss(pred, gt)
     return loss_Seg
      
def main(): 
   
   img_dir= "/projects/sraghavan@xsede.org/GlaucomaIlluminationEnhancement/source_code/Retina-Seg/test_images/"
   model_dir = "/projects/sraghavan@xsede.org/GlaucomaIlluminationEnhancement/source_code/Retina-Seg/model/UNet_Ensemble"
  

   i1 = open_image(f"{img_dir}A.png")
   i2 = open_image(f"{img_dir}B.png")
#    print(f"type of i1 = {type(i1)}")
#    print(f"size of image = {i1.shape}")
#    print(f"size of image = {i2.shape}")

   #loss = SegmentationLoss(model_dir, img_dir)
   loss = SegmentationLoss()

   print(loss(i1, i2))

#main()
#/projects/sraghavan@xsede.org/GlaucomaIlluminationEnhancement/source_code/pytorch-CycleGAN-and-pix2pix-illumination/models/segmentation_loss.py
     
     
     
     
     

